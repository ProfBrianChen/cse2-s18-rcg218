//Ryan Grace
//April 2 2018
//Lab 07 - Random Sentence Generator
//This lab practices the use of methods to generate random sentences from a predetermined set of words 

import java.util.Random; //import random class
public class lab07{ //declare class
  static Random ranGen = new Random(); //create random num generator within public class but outside any methods so it can be reused
  public static void main(String[] args){
    String subject =  phase1(); //Generates random sentence and returns the subject of that sentence
    String directObj = action(subject); //Creates action sentence, returning direct object to be used in conluding sentence
    Conclusion(subject, directObj);
  }
  
  public static String phase1(){ //creates a random sentence and returns the subject of that sentence 
    String adj1 = adj();
    String adj2 = adj();
    String adj3 = adj();
    String noun = noun();
    String verb = verb();
    String noun2 = noun2();
    System.out.println("The "+adj1+" "+adj2+" "+noun+" "+verb+" the "+adj3+" "+noun2+".");
    return noun;
  }
  public static String action(String subject){
    subject = "The "+subject+"";
    int randomInt = ranGen.nextInt(2);
    if (randomInt == 1){
      subject = "It";
    }
    String adj1 = adj();
    String adj2 = adj();
    String adj3 = adj();
    String noun = noun();
    String noun2 = noun();
    String noun3 = noun();
    String verb = verb();
    System.out.println(subject+" was aggressive towards the "+adj2+" "+noun+" who "+verb+" the "+adj1+" "+noun3+".");
    return noun;
  }
  public static void Conclusion(String subject, String directObj){
    String verb = verb();
    System.out.println("That "+subject+" really "+verb+" the "+directObj+"!");
  }
  public static String adj(){
    String adj = "fill";
    int randomInt = ranGen.nextInt(10);
    switch (randomInt){
      case 0:
        adj = "quick";
        break;
      case 1:
        adj = "strong";
        break;
      case 2:
        adj = "incompetent";
        break;
      case 3:
        adj = "small";
        break;
      case 4:
        adj = "hungry";
        break;
      case 5:
        adj = "tired";
        break;
      case 6:
        adj = "gigantic";
        break;
      case 7:
        adj = "embarrasing";
        break;
      case 8:
        adj = "dejected";
        break;
      case 9:
        adj = "sad";
        break;
    }
    return adj;
  }
 public static String noun (){
    String noun = "fill";
    int randomInt = ranGen.nextInt(10);
    switch (randomInt){
      case 0:
        noun = "dog";
        break;
      case 1:
        noun = "man";
        break;
      case 2:
        noun = "woman";
        break;
      case 3:
        noun = "cat";
        break;
      case 4:
        noun = "artist";
        break;
      case 5:
        noun = "student";
        break;
      case 6:
        noun = "professor";
        break;
      case 7:
        noun = "politician";
        break;
      case 8:
        noun = "athlete";
        break;
      case 9:
        noun = "bum";
        break;
    }
    return noun;
  }
 public static String verb (){
   String verb = "fill";
    int randomInt = ranGen.nextInt(10);
    switch (randomInt){
      case 0:
        verb = "watched";
        break;
      case 1:
        verb = "studied";
        break;
      case 2:
        verb = "bounced";
        break;
      case 3:
        verb = "questioned";
        break;
      case 4:
        verb = "observed";
        break;
      case 5:
        verb = "destroyed";
        break;
      case 6:
        verb = "fought";
        break;
      case 7:
        verb = "directed";
        break;
      case 8:
        verb = "engaged";
        break;
      case 9:
        verb = "manipulated";
        break;
    }
    return verb;
  }
public static String noun2 (){
  String noun2 = "fill";
    int randomInt = ranGen.nextInt(10);
    switch (randomInt){
      case 0:
        noun2 = "public";
        break;
      case 1:
        noun2 = "friend";
        break;
      case 2:
        noun2 = "athete";
        break;
      case 3:
        noun2 = "food";
        break;
      case 4:
        noun2 = "animal";
        break;
      case 5:
        noun2 = "magnets";
        break;
      case 6:
        noun2 = "fence";
        break;
      case 7:
        noun2 = "race";
        break;
      case 8:
        noun2 = "direct object of the sentence";
        break;
      case 9:
        noun2 = "human";
        break;
    }
    return noun2;
  }
}