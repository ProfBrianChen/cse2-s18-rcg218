//lab08
//Practice with arrays 


import java.util.Random;
import java.util.Scanner;

public class lab08{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    Random rn = new Random();
    int numStudents = 5 + rn.nextInt(6);
    System.out.println("Class Size: "+numStudents);
    String [] studentNames;
    studentNames = new String[numStudents];
    for (int i=0; i < studentNames.length; i++){
      System.out.println("Enter student name:");
      studentNames[i] = scan.next();
    }
    int scoreSum = 0;
    int grade = 0;
    int [] midterm;
    midterm = new int[numStudents];
    for (int i = 0; i < studentNames.length; i++){
      grade = rn.nextInt(101);
      midterm [i] = grade;
    }
    System.out.println("Student Names and grades:");
    for (int i = 0; i < studentNames.length; i++){
      System.out.println(studentNames[i]+": "+midterm[i]);
      scoreSum += midterm[i];
    }
    double average = scoreSum / numStudents;
    System.out.println("The class average was "+average);
  }
}