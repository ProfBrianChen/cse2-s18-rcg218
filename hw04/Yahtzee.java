//Ryan Grace 
//Feb 19 2018
//Homework 4 - Yahtzee
/* 
  The objective of this homework assignment is to uilize selection statements, operators, and String manipulation to develop a game of Yahtzee.
  The program will perform a random roll of 5 dice for 13 separate rounds or take input from the user in the form of a 5 digit number, and then it will 
  compute the score. 
*/
import java.util.Scanner; //import scanner

public class Yahtzee{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in);
    int lowerScore = 0; //this line and the following are declaring all the variabels necessary for the while loop to run 
    int upperScore = 0;
    int ace = 0, twos = 0, threes = 0, fours = 0, fives = 0, sixes = 0;
    int largeStraight = 0, smallStraight = 0;
    int yahtzee = 0, smallStraightBox = 0, largeStraightBox = 0, fullHouse = 0, fullHouseBox = 0, chance = 0, chanceBox = 0; //Define lower score variables outisde of while loop
    int fourOfaKind = 0, threeOfaKind = 0, fourOfaKindBox = 0, threeOfaKindBox = 0;
    int aceCount = 0, twosCount = 0, threesCount = 0, foursCount = 0, fivesCount = 0, sixesCount = 0;
    int counter = 0; //Used for the while loop
    int rollCount = 0; //Represents the number of times the dice have been rolled
    double diceRoll = 0;
    while ( counter < 13 ){ //Created a while loop to repeat the program for 13 rolls
    System.out.println("Welcome to Yahtzee. Enter 5 numbers that appear on a dice in the form xxxxx. For instance, 14236.");
    System.out.println("If the number is not 5 digits or if it contains values that are impossible for dice (0,7,etc), ");
    System.out.println("then the program will generare them randomly. To generate random numbers enter any number that doesn'y fit the parameters. Enter: ");
    diceRoll = myScanner.nextDouble();
    double die5 = (int) diceRoll % 10; //Using the modulo operator, we isolate each individual character in the 5-digit number the user input
    double die4 = (int) (diceRoll /10) % 10;
    double die3 = (int) (diceRoll / 100) % 10;
    double die2 = (int) (diceRoll / 1000) % 10; 
    double die1 = (int) (diceRoll / 10000) % 10;
    if (diceRoll >= 11111 && diceRoll <= 66666 && die1 >=1 && die1 <=6 && die2 >=1 && die2 <=6 && die3 >=1 && die3 <=6 && die4 >=1 && die4 <=6 && die5 >=1 && die5 <=6){ 
      rollCount++; //The above if statement ensure the users input follows all parameters 
    }
    else {
      System.out.println("Flawed input. Dice will be randomly generated."); //If the users input does not meet the given criteria, the program generates random numbers 
      die1 = (int) (Math.random() * 6) + 1;
      die2 = (int) (Math.random() * 6) + 1;
      die3 = (int) (Math.random() * 6) + 1;
      die4 = (int) (Math.random() * 6) + 1;
      die5 = (int) (Math.random() * 6) + 1;
      diceRoll = (int) ((die1 * 10000) + (die2 * 1000) + (die3 * 100) + (die4 * 10) + die5);
      rollCount++; //Roll count is used below to return the user's input or the randomly generated number after each round of playing
    }
     switch ( rollCount ){  
      case 1: 
        System.out.println("Roll 1: "+diceRoll); //After the first roll, the program produces the 5 digits that were rolled. This is repeated for all 13 rounds of play. 
        break;
      case 2:
        System.out.println("Roll 2: "+diceRoll);
        break;
      case 3:
        System.out.println("Roll 3: "+diceRoll);
        break;
      case 4:
        System.out.println("Roll 4: "+diceRoll);
        break;
      case 5:
        System.out.println("Roll 5: "+diceRoll);
        break;
      case 6:
        System.out.println("Roll 6: "+diceRoll);
        break;
      case 7:
        System.out.println("Roll 7: "+diceRoll);
        break;
      case 8:
        System.out.println("Roll 8: "+diceRoll);
        break;
      case 9:
        System.out.println("Roll 9: "+diceRoll);
        break;
      case 10:
        System.out.println("Roll 10: "+diceRoll);
        break;
      case 11:
        System.out.println("Roll 11: "+diceRoll);
        break;
      case 12:
        System.out.println("Roll 12: "+diceRoll);
        break;
      case 13:
        System.out.println("Roll 13: "+diceRoll);
        break;       
     }
    ace = 0;
    if ( die1 == 1 ){
      ace++; //If the first dice is a one, it adds one to the total. This is repeated five times (once for each die) to get the total number of aces 
    }
    if ( die2 == 1 ){
      ace++;
    }
    if ( die3 == 1 ){
      ace++;
    }
    if ( die4 == 1 ){
      ace++;
    }
    if ( die5 == 1 ){
      ace++;
    }
    //Twos 
    twos = 0;
    if ( die1 == 2 ){
      twos = twos + 2; //If the first dice is a two, it adds two to the total. This is repeated five times (once for each die) to get the total number of twos
    }
    if ( die2 == 2 ){
      twos = twos + 2;
    }
    if ( die3 == 2 ){
      twos = twos + 2;
    }
    if ( die4 == 2 ){
      twos = twos + 2;
    }
    if ( die5 == 2 ){
      twos = twos + 2;
    }
    //Threes
    threes = 0;
    if ( die1 == 3 ){
      threes = threes + 3; //If the first dice is a three, it adds three to the total. This is repeated five times (once for each die) to get the total number of threes 
    }
    if ( die2 == 3 ){
      threes = threes + 3;
    }
    if ( die3 == 3 ){
      threes = threes + 3;
    }
    if ( die4 == 3 ){
      threes = threes + 3;
    }
    if ( die5 == 3 ){
      threes = threes + 3;
    }
    //Fours
    fours = 0;
    if ( die1 == 4 ){
      fours = fours + 4; //If the first dice is a four, it adds four to the total. This is repeated five times (once for each die) to get the total number of fours 
    }
    if ( die2 == 4 ){
      fours = fours + 4;
    }
    if ( die3 == 4 ){
      fours = fours + 4;
    }
    if ( die4 == 4 ){
      fours = fours + 4;
    }
    if ( die5 == 4 ){
      fours = fours + 4;
    }
    //Fives
    fives = 0;
    if ( die1 == 5 ){
      fives = fives + 5; //If the first dice is a five, it adds five to the total. This is repeated five times (once for each die) to get the total number of fives 
    }
    if ( die2 == 5 ){
      fives = fives + 5;
    }
    if ( die3 == 5 ){
      fives = fives + 5;
    }
    if ( die4 == 5 ){
      fives = fives + 5;
    }
    if ( die5 == 5 ){
      fives = fives + 5;
    }
    //Sixes
    sixes = 0;
    if ( die1 == 6 ){
      sixes = sixes + 6; //If die1 is a 6, it adds 6 to the total number of "sixes". This is repeated for each additional die to get the total number of sixes. 
    }
    if ( die2 == 6 ){
      sixes = sixes + 6;
    }
    if ( die3 == 6 ){
      sixes = sixes + 6;
    }
    if ( die4 == 6 ){
      sixes = sixes + 6;
    }
    if ( die5 == 6 ){
      sixes = sixes + 6;
    }
    //Lower Score Calculations
    //Yahtzee
    if ( (diceRoll % 11111) == 0 && yahtzee == 0 ){ //If the user scores Yahtzee, their lower Score is increased by 50 points
      yahtzee++; //the increment ensures that Yahtzee can't be scored again 
      System.out.println("YAHTZEE!!!");
      }
    //Small Straight
    if (smallStraightBox == 0 ){
     if (die1 == 1 || die2 == 1 || die3 == 1 || die4 == 1 || die5 == 1){ //Checks to see if box one contains one of the 4 potential numbers
      if (die1 == 2 || die2 == 2 || die3 == 2 || die4 == 2 || die5 == 2){//Continues checking each die for the next sequential number
        if (die1 == 3 || die2 == 3 || die3 == 3 || die4 == 3 || die5 == 3){
          if (die1 == 4 || die2 == 4 || die3 == 4 || die4 == 4 || die5 == 4){
            smallStraight = 30; //If the nested if statements prove that there is a four digit straight (small straight), the variable small straight is assigned 30 points
            smallStraightBox++; //small straight box is incremented plus one. It is now no longer equal to zero, making small straights impossible in future iterations
            System.out.println("You got a small straight!");
            }
          }
        }
      }
    }
     if (smallStraightBox == 0 ){
        if (die1 == 2 || die2 == 2 || die3 == 2 || die4 == 2 || die5 == 2){
          if (die1 == 3 || die2 == 3 || die3 == 3 || die4 == 3 || die5 == 3){
            if (die1 == 4 || die2 == 4 || die3 == 4 || die4 == 4 || die5 == 4){
              if (die1 == 5 || die2 == 5 || die3 == 5 || die4 == 5 || die5 == 5){
                smallStraight = 30;
                smallStraightBox++;
                System.out.println("You got a small straight!");
              }
            }
          }
        }
     }
      if (smallStraightBox == 0 ){
        if (die1 == 3 || die2 == 3 || die3 == 3 || die4 == 3 || die5 == 3){
          if (die1 == 4 || die2 == 4 || die3 == 4 || die4 == 4 || die5 == 4){
            if (die1 == 5 || die2 == 5 || die3 == 5 || die4 == 5 || die5 == 5){
              if(die1 == 6 || die2 == 6 || die3 == 6 || die4 == 6 || die5 == 6){
                smallStraight = 30;
                smallStraightBox++;
                System.out.println("You got a small straight!");
              }
            }
          }
        }
      }
    //Large Straight
    if (smallStraightBox == 1 && largeStraightBox == 0 ){
      if (die1 == 1 || die2 == 1 || die3 == 1 || die4 == 1 || die5 == 1){ 
        if (die1 == 2 || die2 == 2 || die3 == 2 || die4 == 2 || die5 == 2){
          if (die1 == 3 || die2 == 3 || die3 == 3 || die4 == 3 || die5 == 3){
            if (die1 == 4 || die2 == 4 || die3 == 4 || die4 == 4 || die5 == 4){
              if (die1 == 5 || die2 == 5 || die3 == 5 || die4 == 5 || die5 == 5){
                largeStraight = 40;
                largeStraightBox++;
                System.out.println("You got a large straight!");
              }
            }
          }
        }
      }
    }
     if (largeStraightBox == 0){
      if (die1 == 2 || die2 == 2 || die3 == 2 || die4 == 2 || die5 == 2){
        if (die1 == 3 || die2 == 3 || die3 == 3 || die4 == 3 || die5 == 3){
          if (die1 == 4 || die2 == 4 || die3 == 4 || die4 == 4 || die5 == 4){
            if (die1 == 5 || die2 == 5 || die3 == 5 || die4 == 5 || die5 == 5){
              if(die1 == 6 || die2 == 6 || die3 == 6 || die4 == 6 || die5 == 6){
                largeStraight = 40;                
                largeStraightBox++;
                System.out.println("You got a large straight!");
              }
            }
          }
        } 
      }
     }

    //4 of a kind
    if ( fourOfaKindBox == 0){
      if ( (sixes / 6 ) >= 4){
        fourOfaKind = 24;
      }
      else if ( (fives / 5) >= 4){
        fourOfaKind = 20;
      }
      else if ( (fours /  4) >= 4){
        fourOfaKind = 16;
      }
      else if ( (threes / 3) >= 4){
        fourOfaKind = 12;
      }
      else if ( (twos / 2) >= 4 ){
        fourOfaKind = 8;
      }
      else if ( (ace / 1) >= 4){
        fourOfaKind = 4;
      }
      if ( fourOfaKind > 0 ){
        System.out.println("You got four of a kind!");
        fourOfaKindBox++;
      }
   }
    //3 of a kind
    if ( (sixes / 6 ) >= 3){
      threeOfaKind = 18;
    }
    else if ( (fives / 5) >= 3){
      threeOfaKind = 15;
    }
    else if ( (fours /  4) >= 3){
      threeOfaKind = 12;
    }
    else if ( (threes / 3) >= 3){
      threeOfaKind = 9;
    }
    else if ( (twos / 2) >= 3 ){
      threeOfaKind = 6;
    }
    else if ( (ace / 1) >= 3){
      threeOfaKind = 3;
    }
    if ( threeOfaKind > 0 ){
      System.out.println("You got three of a kind!");
      threeOfaKindBox++;
    }
    //Full House 
    //First find out if there are any pairs
    int pairs = 0;
    if ((sixes / 6) == 2 || (fives / 5) == 2 || (fours / 4) == 2 || (threes / 3) == 2 || (twos / 2) == 2 || (ace / 1) == 2){
      pairs = 1;
    }
    if (threeOfaKind > 0 && pairs == 1){
      fullHouseBox++;
      System.out.println("You got a Full House!");
    }
    //Chance
    chance = (int) (die1 + die2 + die3 + die4 + die5);
    //Lower Section Calculation
    //Lower Section Calculation accounts for Yahtzee (50 points), large straights (40 points), small straights (30 points), full houses (25 points), 4 of a kind, 3 of a kind, and 3 of a kind
    if ( yahtzee == 1 ){
      lowerScore = lowerScore + 50;
      fourOfaKindBox = 0; //Because the parameters of four and three-of-a-kind rolls fit within a Yahtzee roll, it is necessary to reset them to avoid double counting 
      threeOfaKindBox = 0;
      yahtzee++;
    }
    else if ( largeStraightBox == 1 ){ //By meeting the parameter earlier in the code, largeStraightBox went from 0 to 1. To avoid double counting we now increment it again, going from 1 to 2.
      lowerScore = lowerScore + 40;    //This strategy of incrementing each of the lower variables twice is repeated again for small straight, full house, 3 of a kind, 4 of a kind, and upper scores   
      largeStraightBox++;
    }
    else if ( smallStraightBox == 1 ){
      lowerScore = lowerScore + 30;
      smallStraightBox++;
    }
    else if ( fullHouseBox == 1 ){
      lowerScore = lowerScore + 25;
      fullHouseBox++;
    }
    else if ( fourOfaKindBox == 1 && threeOfaKindBox == 1 ){
      lowerScore = lowerScore + fourOfaKind;
      fourOfaKindBox++;
      threeOfaKindBox = 0;
    }
    else if ( threeOfaKindBox == 1 ){
      lowerScore = lowerScore + threeOfaKind;
      threeOfaKindBox++;
    }
    else if ( chanceBox == 0 ){
      lowerScore = lowerScore + chance;
      chanceBox++;
    }
    else if (ace > 0 && aceCount == 0) {
      upperScore = upperScore + ace;
      aceCount++;
    }
    else if (twos > 0 && twosCount == 0){
      upperScore = upperScore + twos;
      twosCount++;
    }
    else if (threes > 0 && threesCount == 0){
      upperScore = upperScore + twos;
      threesCount++;
    }
    else if (fours > 0 && foursCount == 0 ){
      upperScore = upperScore + fours;
      foursCount++;
    }
    else if (fives > 0 && fivesCount == 0){
      upperScore = upperScore + fives;
      fivesCount++;
    }
    else if (sixes > 0 && sixesCount == 0){
      upperScore = upperScore + sixes;
      sixesCount++;
    }
   
    System.out.println("Total Lower Score after this round: "+lowerScore);
    System.out.println("Total Upper Score after this round: "+upperScore);
    System.out.println("Three of a Kind equals: "+threeOfaKind);
    System.out.println("Four of a kind equals: "+fourOfaKind);
    System.out.println("Chance equals: "+chance);

    counter++;
  }
    System.out.println("The total lower score was "+lowerScore);
    System.out.println("The total upper score before the potential bonus was "+upperScore);
    int upperScoreBonus = 0;
    if (upperScore > 63){
      upperScoreBonus = upperScore + 35;
      System.out.println("You recieved the bonus! This brings your total upper score to "+upperScoreBonus);
    }
    int grandTotal = lowerScore + upperScoreBonus;
   
    System.out.println("The total upper score with the bonus was "+upperScoreBonus);
    System.out.println("The grand total after thirteen dice rolls was "+grandTotal);
  }
}