=============================
grade6.txt
=============================
Grade: 60/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
The code does not compile

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
N/A

C) How can any runtime errors be resolved?
N/A

D) What topics should the student study in order to avoid the errors they made in this homework?
N/A

E) Other comments:
The code doesn't run, there aren't any comments, and you have careless mistakes for what you did submit.