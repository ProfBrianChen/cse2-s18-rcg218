public class WelcomeClass {
  public static void main(String[] args){
    //prints user id and short bio
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^   ^   ^   ^   ^   ^");
    System.out.println("/ \\ / \\ / \\ / \\ / \\ / \\");
    System.out.println(" r   c   g   2   1   8");
    System.out.println("\\ / \\ / \\ / \\ / \\ / \\ /");
    System.out.println(" v   v   v   v   v   v ");
    System.out.println("My name is Ryan and I am a senior IR/econ major at Lehigh");
  }
}