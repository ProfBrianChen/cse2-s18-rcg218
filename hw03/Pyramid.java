//Ryan Grace
//cse hw03
//Feb 10th 2018
//This program takes input from the user and returns the volume of a square pyramid
import java.util.Scanner; //import scanner 

public class Pyramid {
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Enter the height of the pyramid (in the form of xx.xx): "); //Program requests input from the user
    double pyramidHeight = myScanner.nextDouble(); //User inputs the height of the pyramid
    System.out.println("Enter the length of the pyramid base (in the form of xx.xx): "); //Program requests input from the user
    double pyramidLength = myScanner.nextDouble(); //user inputs the length of the pyramid's base
    double pyramidVolume = (pyramidLength * pyramidLength) * pyramidHeight / 3; //Formula for the volume of a square pyramid
    System.out.println("The volume of the pyramid is "+pyramidVolume+""); //Program outputs the volume 
  }
}
    