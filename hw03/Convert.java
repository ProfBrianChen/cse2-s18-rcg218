//Ryan Grace
//cse hw03
//Feb 10th 2018
//This program takes input from the user in the form of doubles and converts it into square miles
import java.util.Scanner; //import scanner 

public class Convert {
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Enter the affected area in acres (in the form xx.xx):");
    double acres = myScanner.nextDouble();
    System.out.println("Enter the average number of inches in rainfall (in the form xx.xx):");
    double rainInches = myScanner.nextDouble();
    double base = acres / 640; //There are 640 acres in a square mile
    double height = rainInches / 63360; //There are 633360 inches in a mile. This converts the inches of rainfall into miles 
    double cubicMiles = base * height; 
    System.out.println("The total rainfall was "+cubicMiles+" cubic miles."); //print out results of the calculation in cubic miles
  }
}