/*
Ryan Grace
Homework #7
Program: StringAnalysis
This program examines a string to see if the characters in a string are letters or not 
*/
import java.util.Scanner; //import scanner 

public class StringAnalysis { //declare public class StringAnalysis 
  static Scanner scan = new Scanner(System.in); //name scanner
  
  public static Boolean StrungOut ( String input, int charNum ){ //declare first method 
    System.out.println("The one with the int!");
    boolean check = false; //intialize bool variable to be returned 
    int j = 0; 
    if ( charNum > input.length() ){ //This if statement makes sure the for loop does not run too long
      charNum = input.length();
    }
    for (int i = 0; i < charNum; i++){ //For loop runs until either the user's chosen number or the end of the string is reached
      char q = input.charAt(i);
      if ( q >= 'a' && q <= 'z' || q >= 'A' && q <= 'Z' ){ //Checks to see if letters are in the 
        j++; //Increments if the char in a given slot is a letter (counts the letters in a string) 
      }
    }
    if ( j == charNum ){ 
      check = true; //If the number of letters in the string (j) is equal to the number of chars in the string, the var returns true
    }
    else {
      check = false; //If there are non-letter characters, the var returns false 
    }
    return check; //Returns bool variable
  }
  
  public static Boolean StrungOut ( String input, double fill ){
    //Method checks for String (user's input) and a double, which occurs automatically and would only become an int should the user input a limit on the string
    boolean check = false;
    int j = 0; //Start j at negative because the final iteration of the for loop will increment one more time 
    for (int i = 0; i < (input.length()); i++){
    char q = input.charAt(i);
      if ( q >= 'a' && q <='z' || q >= 'A' && q <= 'Z'){ //Checks if user input is a letter
        j++;
        }
    }
      if ( j == input.length() ){
        check = true;
      }
      else{
        check = false;
      }
    return check;
  } 
  public static void main(String[] args){ //Main method
    int charNum = -5; //Declare charNum, which will only change if the user inputs a desired num. of chars to be checked
    boolean check1 = false; //declare bool variable 
    double fill = 3.3; //declare double val which will act as a filler (no real purpose other than to signify which method to use)
    System.out.println("This program tells you if a string contains only letters. You can test the whole string or portion of the string.");
    System.out.println("Do you want to select a limited portion of the string? If yes, type the following: yes or Yes. If not, type no.");
    String spec = scan.next(); //Prompts user for yes/no answer
    if (spec.equals("Yes") || spec.equals("yes")){
      System.out.println("How many characters do you want the program to check? (Enter an integer)");
      while ( !scan.hasNextInt() ){ //Checks to see if user inputs an integer
        System.out.println("Bad input, enter an integer please.");
        scan.next();
      }
      charNum = scan.nextInt(); //Reassigns double created on line 52 to an int. This is important to ensure the correct method is used
    }
    System.out.println("Please input a string and this program will tell you if it's all letters or not.");
    String input = scan.next();
    if ( charNum > -1 ){
    check1 = StrungOut(input, charNum); //use method with String and Int
    }
    else {
    check1 = StrungOut(input, fill); //use method with String and double 
    }
    if (check1){ //If either method returns true, the following is printed
      System.out.println("The string (or portion of the string you chose) only contained letters.");
    }
    else { //If either method doesn't return true, the following is printed
      System.out.println("The string (or portion of the string you chose) contained characters other than letters.");
    }
  }
}