/*
Ryan Grace
Homework #7
Program: Area.java
This program writes various methods to calculate the area of a triagle, square, rectangle.
*/
import java.util.Scanner; //miport scanner 

public class Area { //declare public class area 
  static Scanner scan = new Scanner(System.in); //name scanner
  public static double triangle ( double height, double base ){ //method for calulating triangle 
    double area1 = height * base * 0.5; //equation for area of a triangle
    return area1; //returns new variable
  }
  public static double circle ( double radius ){ //declae method for calculating circle area`
    double pi = 3.14159; //declare constant Pi
    double area2 = pi * radius * radius; //equation for area of a circle
    return area2; //returns double area2
  }
  public static double rectangle (double length, double width ){ //Method for area of a rectangle
    return length * width; //equation for rectangle
  }
  public static void main(String[] args){ //Main method
    while (true){ //Infinite loop 
     System.out.println("What shape would the user like to calculate the area for? You may choose triangle, square, or circle.");// prompt use for input
     String input = scan.next(); //Accepts user input 
     if ( input.equals("triangle") || input.equals("Triangle") ){ //if user input satisfies conditions put forward, program continues 
       System.out.println("Please enter a double value for the base of the triangle."); //prompts user for first double
      while (!scan.hasNextDouble()){ //checks to ensure input is a double. If not, the while loop clears scanner and prompts user again 
         System.out.println("Bad input, please try again.");
         scan.next();
      }
      double b = scan.nextDouble(); //assigns user input 
      System.out.println("Please enter a value for the height of the triangle."); //second value for calculating area 
      while (!scan.hasNextDouble()){ //checks to see if input is a double. If not, clears scanner and prompts usre again 
         System.out.println("Bad input, please try again.");
         scan.next();
      }
      double h = scan.nextDouble(); //assigns input to variable`
      double triArea = triangle (b, h); //calls method 
      System.out.println("The area of the triangle is "+triArea); //prints out results from method
       break;
     }
     else if ( input.equals("circle") || input.equals("Circle") ){ //if user input satisfies this condition, the program continues 
       System.out.println("Please enter a double value for the radius of the circle."); //prompts user for radius of circle
       while (!scan.hasNextDouble()){ //checks to make sure user's input is appropriate
         System.out.println("Bad input. Please enter a double value for the radius of the circle.");
         scan.next(); //clears scanner
       }
       double r = scan.nextDouble();
       double circArea = circle (r); //runs method circle
       System.out.println("The area of the circle is "+circArea); //prints out results of the method
       break;
      }
      else if ( input.equals("rectangle") || input.equals("Rectangle") ){ //Checks to see if user input satisfies 
        System.out.println("Please enter a double value for the length of the rectangle."); //prompts use for input 
        while (!scan.hasNextDouble()){ //checks user input 
        System.out.println("Bad input. Please enter a double value for the length of the rectangle.");
        scan.next();
        }
        double l = scan.nextDouble(); //assigns new input 
        System.out.println("Please enter a value for the width of the rectangle.");
        while (!scan.hasNextDouble()){ //checks user input 
          System.out.println("Bad input. Please enter a double value for the width of the rectangle.");
          scan.next();
        }
        double w = scan.nextDouble();
        double rectArea = rectangle(l, w); //runs method rectangle
        System.out.println("The area of the rectangle is "+rectArea); //prints result of method 
        break;
      }
      else { //If initial user input for which shape to calculate for does not satisfy prior conditions from if statements, program requests new input and clears scanner
        System.out.println("Bad input. Try again and make sure to type the name of the shape correctly.");
        scan.next();
      }
    }
  }
}