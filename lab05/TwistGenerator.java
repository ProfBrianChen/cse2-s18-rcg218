//Ryan Grace
//March 2 2018
//This program utilizes while loops to print out a simple twist design, which looks like:
/*
          \ /\ /
           X  X
          / \/ \
 */

import java.util.Scanner; //import scanner

public class TwistGenerator{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); //create scanner
    int length;
    while ( true ){ //create infinite loop
      System.out.println("Enter the length of the twist as an integer: ");
      if (myScanner.hasNextInt()){ //Test to see if input is an integer
        length = myScanner.nextInt(); //If it is, it will be assigned to variable length 
        break; //leave the infinite loop
      }
      else {
        System.out.println("Error. Bad input.");
        String junk = myScanner.next(); //If the user input isn't an int, String junk clears the scanner for new input and the loop restarts
      }
    }
    int count = 0; //Create counter variable, set to zero 
    while (count < length){ //Set limits on the loop's length based on user input 
      if (count % 3 == 0){ //If the modulus operator returns zero, the program prints what belongs in the first position of the twist
        System.out.print("\\");
        count++; //add to count 
      }
      else if (count % 3 == 1){ //Fills second position of first line of the twist
        System.out.print(" ");
        count++;
      }
      else if (count % 3 == 2){
        System.out.print("/"); //Fills third position of first line of the twist, as the count grows higher it cycles back to first position
        count++;
      }
    }
    System.out.println(); //Prints a new line for the next row of the twist. Needed bc the previous output printed on the same line
    count = 0; //Reset the counter at zero so the program does not skip the second and third rows of the twist
    while (count < length){ //Repeats the same process for the first position in row 2 
      if (count % 3 == 0){
        System.out.print(" ");
        count++;
      }
      else if (count % 3 == 1){ //Repeats the same process as row 1 
        System.out.print("X");
        count++;
      }
      else if (count % 3 == 2){ //Repeat of previously seen process
        System.out.print(" ");
        count++;
      }
    }
    System.out.println(); //Print new line so the next set of output can print on a different line but also print together
    count = 0;
    while (count < length){   //same process, repeated again. In fact, it's just a mirroring of the first row of the twist
      if (count % 3 == 0){
        System.out.print("/");
        count++;
      }
      else if (count % 3 == 1){
        System.out.print(" ");
        count++;
      }
      else if (count % 3 == 2){
        System.out.print("\\");
        count++;
      }
    }
    System.out.println();
  }
}
