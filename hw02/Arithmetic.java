//Ryan Grace
//Homework 2: Arithmetic Calculations
//Feb 4th 2018
//Objective: Practice manipulating data stored in variables
//Task: Compute costs of items purchased

public class Arithmetic{
  public static void main(String[] args){
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98; //Price per pants
    int numShirts = 2; //number of sweatshirts
    double shirtsPrice = 24.99; //price per sweatshirt
    int numBelts = 2; //Number of Belts
    double beltPrice = 33.99; //Price per belt
    double paSalesTax = 0.06; //Pennsylvania Sales Tax of 6%
    
    //Declare variables for Total Cost of Each Kind of Item without tax
    double totalCostOfPants = ((int) (100 * numPants * pantsPrice)) / 100.0; //Cast as int as a way of truncating decimals so it looks cleaner
    double totalCostOfShirts = ((int) (100 * numShirts * shirtsPrice)) / 100.0;
    double totalCostOfBelts = ((int) (100 * numBelts * beltPrice)) / 100.0;
    //Declare Variables for Sales Tax incurred by buying all of each item
    double taxOnPants = ((int)(100 * totalCostOfPants * paSalesTax)) / 100.0;
    double taxOnShirts = ((int) (100 * totalCostOfShirts * paSalesTax)) / 100.0;
    double taxOnBelts = ((int) (100 * totalCostOfBelts * paSalesTax)) / 100.0;
    //Declare Variable for Total Cost of items (without tax) and total sales tax 
    double pantsWithTax = ((int) (100 * (totalCostOfPants + taxOnPants))) / 100.0;
    double shirtsWithTax = ((int) (100 * (totalCostOfShirts + taxOnShirts))) / 100.0;
    double beltsWithTax = ((int) (100 * (totalCostOfBelts + taxOnBelts))) / 100.0;
    double totalNoTax = ((int) (100 * (totalCostOfBelts + totalCostOfShirts + totalCostOfPants))) / 100.0;
    double totalSalesTax = ((int) (100 * (taxOnBelts + taxOnShirts + taxOnPants))) / 100.0;
    //Declare Variable for Total Cost, tax included
    double totalCost = ((int) (100 * (totalNoTax + totalSalesTax))) / 100.0;
    //Display cost of each item type and sales tax paid for each 
    System.out.println("The total cost of pants before tax was $"+totalCostOfPants+".");
    System.out.println("The total sales tax on pants was $"+taxOnPants+".");
    System.out.println("The cost of pants including tax was $"+pantsWithTax+".");
    System.out.println(" ");//Inserted blank lines so the printed result looked nicer
    System.out.println("The total cost of sweatshirts before tax was $"+totalCostOfShirts+".");
    System.out.println("The total sales tax on sweatshirts was $"+taxOnShirts+".");
    System.out.println("The cost of sweatshirts including tax was $"+shirtsWithTax+".");
    System.out.println(" ");
    System.out.println("The total cost of belts before tax was $"+totalCostOfBelts+".");
    System.out.println("The total sales tax on belts was $"+taxOnBelts+".");
    System.out.println("The cost of belts including tax was $"+beltsWithTax+".");
    System.out.println(" ");
    System.out.println("The total cost of all purhcases before sales tax was $"+totalNoTax+".");
    System.out.println("The total sales tax was $"+totalSalesTax+".");
    System.out.println("The total cost of the purchases (including tax) was $"+totalCost+".");
                        
  }
}