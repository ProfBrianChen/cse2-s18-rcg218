//Ryan Grace 
//April 15 2018
//Homework 09 deals two hands of poker

import java.util.Random; //import Random object 

public class DrawPoker{
  static Random ranNum = new Random(); //name the random num generator 
  public static void main(String[] args){
    int hand[] = new int[10]; //create new array 
    fill(hand); //run fill method to populate the array
    System.out.println();
    int first[] = new int[5]; //create new array for first hand 
    int second[] = new int [5]; //create new array for second hand
    int a = 0; 
    int k = 0;
    for (int i = 0; i < hand.length; i++){ //This for loop populates the two hands with the 10 randomly generated no.'s from previous array 
      if (i % 2 == 0){ //Puts all even position numbers in the array into first hand  and odd ones into second hand
        first[a] = hand[i];
        a++;
      }
      else{
        second[k] = hand[i];
        k++;
      }
    }
    String suit1 = " ", suit2 = " ", num1 = " ", num2 = " ";
    System.out.println("The first hand was: ");
    for (int i = 0; i < first.length; i++){ //This loop assigns suit and card number to the first hand 
      int suits = first[i] / 13; //Dividing by 13 gives 4 possible outcomes, one for each suit 
      int cardNum = first[i] % 13; //Modulus operator gives thirteen potential outcomes for each card 
      switch (suits){
        case 0:
          suit1 = "Spades";
          break;
        case 1:
          suit1 = "Clubs";
          break;
        case 2:
          suit1 = "Diamonds";
          break;
        case 3:
          suit1 = "Hearts";
          break;
      }
      switch (cardNum){
        case 0:
          num1 = "Ace";
          break;
        case 1:
          num1 = "Two";
          break;
        case 2:
          num1 = "Three";
          break;
        case 3:
          num1 = "Four";
          break;
        case 4:
          num1 = "Five";
          break;
        case 5:
          num1 = "Six";
          break;
        case 6:
          num1 = "Seven";
          break;
        case 7:
          num1 = "Eight";
          break;
        case 8:
          num1 = "Nine";
          break;
        case 9:
          num1 = "Ten";
          break;
        case 10:
          num1 = "Jack";
          break;
        case 11:
          num1 = "Queen";
          break;
        case 12:
          num1 = "King";
          break;
      }
      System.out.println(num1+" of "+suit1);
    }
    System.out.println();
    System.out.println("The second hand was: ");
      for (int i = 0; i < second.length; i++){ //I repeat the code used above for hand two. In retrospect I def should've made this a 
        int suits = second[i] / 13;            // separate method
        int cardNum = second[i] % 13;
        switch (suits){
          case 0:
            suit2 = "Spades";
            break;
          case 1:
            suit2 = "Clubs";
            break;
          case 2:
            suit2 = "Diamonds";
            break;
          case 3:
            suit2 = "Hearts";
            break;
        }
        switch (cardNum){
          case 0:
            num2 = "Ace";
            break;
          case 1:
            num2 = "Two";
            break;
          case 2:
            num2 = "Three";
            break;
          case 3:
            num2 = "Four";
            break;
          case 4:
            num2 = "Five";
            break;
          case 5:
            num2 = "Six";
            break;
          case 6:
            num2 = "Seven";
            break;
          case 7:
            num2 = "Eight";
            break;
          case 8:
            num2 = "Nine";
            break;
          case 9:
            num2 = "Ten";
            break;
          case 10:
            num2 = "Jack";
            break;
          case 11:
            num2 = "Queen";
            break;
          case 12:
            num2 = "King";
            break;
        }
        System.out.println(num2+" of "+suit2);
      }
    //Insert all the checks
    System.out.println();
    boolean pairCheck1 = pair(first); //Checks for pair in hand one 
    boolean tripCheck1 = threeOfAkind(first); //Checks for three of a kind
    boolean flushCheck1 = flush(first); //Checks for flush 
    boolean houseCheck1 = fullHouse(first); //Checks for full house 
    int highNum1 = highCard(first); //Evaluates high card for comparison later
    
    boolean pairCheck2 = pair(second); //Repeat of above, but for the second hand 
    boolean tripCheck2 = threeOfAkind(second);
    boolean flushCheck2 = flush(second);
    boolean houseCheck2 = fullHouse(second);
    int highNum2 = highCard(second);
    
    String one = "First hand wins!";
    String two = "Second hand wins!";
    while (true){ //This while loop evaluates the above booleans and prints output according to the winning hand
      if (houseCheck1 && !houseCheck2){ 
        System.out.println(one);
        break;
      }
      else if (!houseCheck1 && houseCheck2){
        System.out.println(two);
        break;
      }
      else if (flushCheck1 && !flushCheck2){
        System.out.println(one);
        break;
      }
      else if (!flushCheck1 && flushCheck2){
        System.out.println(two);
        break;
      }
      else if (tripCheck1 && !tripCheck2){
        System.out.println(one);
        break;
      }
      else if (!tripCheck1 && tripCheck2){
        System.out.println(two);
        break;
      }
      else if (tripCheck1 && tripCheck2){
        System.out.println("Higher three of a kind wins!"); //Because we returned boolean values not integers, we can't tell who has the high trips
      }
      else if (pairCheck1 && !pairCheck2){
        System.out.println(one);
        break;
      }
      else if (!pairCheck1 && pairCheck2){
        System.out.println(two);
        break;
      }
      else if (pairCheck1 && pairCheck2){
        System.out.println("Higher pair wins!"); //We returned boolean, so the user has to evaluate the higher of the two pairs 
        break;
      }
      else if (highNum1 > highNum2){
        System.out.println(one);
        break;
      }
      else if (highNum1 < highNum2){
        System.out.println(two);
        break;
      }
      else if (highNum1 == highNum2){
        System.out.println("It's a draw!");
        break;
      }
    }
  }
  
  public static void fill(int[] hand){  //Method fill populates the 10 digit array and ensures no duplicates 
    for (int i = 0; i < hand.length; i++){
      hand[i] = ranNum.nextInt(52);
      int j =0;
      if (i != 0){
        for (j = 0; j < i; j++){
          if (hand[i] == hand[j]){
            i--; //Runs the loop again and makes sure there are no duplicates 
            continue;
          }
        }
      }
    }
    return;
  }
  public static boolean pair(int[] input){
    boolean check = false; 
    int count = 0; 
    for (int i = 0; i < input.length; i++){
      for (int j = 0; j < input.length; j++ ){
        if (i != j && (input[i] % 13 == input[j]  % 13)){ //As long as counters (i & j) are not equal, it checks for matches 
          check = true;
        }
      }
    }
    return check;  
  }
  public static boolean threeOfAkind(int[] input){
    boolean check = false;
    int count = 0;
    for (int i = 0; i < input.length; i++){
      for (int j = 0; j < input.length; j++){
        if (i != j && (input[i] % 13 == input[j] % 13)){
          count++;
        }
      }
    }
    if (count == 6){ //Uses same for loop as the pair, but uses a counter. I use 6 as the counter's designation bc its the only one that works with just three of a kind (two pair returns 4 and full house returns 8)
      check = true; 
    }
    return check;
  }
  public static boolean flush(int[] input){
    boolean check = false;
    int count = 0;
    for (int i = 0; i < input.length; i++){
      if ((input[0]/13) == (input[i]/13)){ //Checks to see if each card returns the same suit as the first card 
        count++; //For each match, the counter increases one
      }
    }
    if (count == 5){ //If count reaches 5 (all cards are same suit), the check becomes true -- user gets a flush 
      check = true;
    }
    return check;
  }
  public static boolean fullHouse(int[] input){
    boolean check = false; 
    int count = 0;
    for (int i = 0; i < input.length; i++){
      for (int j = 0; j < input.length; j++){
        if (i != j && (input[i] % 13 == input[j] % 13)){
          count++;
        }
      }
    }
    if (count == 8){ //Uses same methodology as the threeOfaKind method, but a different counter value 
      check = true;
    }
    return check;
  }
  public static int highCard(int[] input){
    int max = 0;
    for (int i = 0; i < input.length; i++){
      if ((input[i] % 13)> max){ //Evaluates the card number or face value to return the highest 
        max = input[i];
      }
      if (input[i] % 13 == 0){ //Because ace was associated with the remainder 0, but has the highest value, I wrote an exception 
        max = 13;
        break;
      }
    }
    return max;
  }
}
