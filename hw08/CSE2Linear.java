//Ryan Grace 
//April 9th 2018
//Homework 08

import java.util.Random; //import random num generator
import java.util.Scanner; //import scanner
 
public class CSE2Linear{ //declare class
  static Scanner scan = new Scanner(System.in); //Name scanner and random num gen outside main class so it can be used across methods
  static Random ranNum = new Random();
  public static void main(String[] args){
    int classSize = 15; //Declare known class size as literal
    int [] grades; //declare array 
    int num = 0;
    int nextNum = 0;
    grades = new int[classSize]; //Allocate memory to array 
    System.out.println("Enter 15 ascending ints for final grades in CSE2:"); //request input
    for (int i = 0; i < grades.length; i++ ){
      if (!scan.hasNextInt()){
        System.out.println("Bad input. Please enter only integers.");
        scan.next();
        i--; //to ensure we still get 15 total variables, I decrement int i when bad input is introduced
        continue;
      }
      nextNum = scan.nextInt(); //If initial input is an integer, it is delcared as nextNum
      if (nextNum < num){ //Checks to ensure numbers are ascending 
        System.out.println("Numbers need to be ascending.");
        i--;
        continue;        
      }      
      if ( nextNum < 0 || nextNum > 100){
        System.out.println("Error. Not in range.");
        i--;
        continue;
      }
      num = nextNum;   //These two lines ensure that we can check to see if the user's input is ascending 
      grades[i] = num; //Populates array 
    }
    for (int i = 0; i < grades.length; i++){
      System.out.print(grades[i]+" "); //Prints array values 
    }
    System.out.println();
    System.out.println("Enter a grade to be searched."); //Prompts user for input 
    int key = scan.nextInt(); //I am assuming they input an integer
    binarySearch(grades, key); //calls binary method
    System.out.println("Scrambled Array: "); 
    scrambled(grades);
    for (int j = 0; j < grades.length; j++){ //Prints scrambled array 
      System.out.print(grades[j]+" ");
    }
    System.out.println();
    System.out.println("Enter a grade to search for:"); //Prompts user for linear search key 
    int linearKey = scan.nextInt(); 
    linearSearch(grades, linearKey);
  }
  public static void scrambled (int [] jumble){ //Method scrambles user input 
    for (int i = 0; i < jumble.length; i++){
      int randomPlace = ranNum.nextInt(jumble.length); //Generates random number 
      int temp = jumble[i]; //Temp holds value array at the given place (i)
      jumble [i] = jumble[randomPlace]; //Assigns randomly located array value to this iteration's place
      jumble [randomPlace] = temp;
    }
  }
  public static void linearSearch (int[] scrambled, int linearKey){
    boolean check = false; //Creates boolean variable to check and see if requested value is in list 
    for (int i = 0; i < scrambled.length; i++){
      if (scrambled[i] == linearKey){ //Checks each place in the array for the requested value 
        check = true; 
        break; //escapes for loop if appropriate value is found
      }
    }
    if (check == true){  //Prints out whether or not value was found
      System.out.println(linearKey+" was found in the list.");
    }
    else{
      System.out.println(linearKey+" was not found in the list.");
    }
  }
  public static void binarySearch (int [] grades, int key){
    int start = 0; 
    int end = grades.length-1; 
    for (int i = 1; i < grades.length; i++){
      int mid = (start + end) / 2; //Finds middle value of array 
      if (key == grades[mid]){ //Checks to see if requested value is equal to mid place 
        System.out.println("Your input, "+key+", was found.");
        break;
      }
      if (key < grades[mid]){ //if key is less than the value at mid, then mid becomes the top end 
        end = mid;
        continue;
      }
      else { //If key is more than mid value, then the mid value becomes the bottom end
        start = mid;
      }
      if (i == grades.length - 1){ //If the binary search runs to the end of the list and can't find it, it's not there. 
        System.out.println("Your input, "+key+", was not found.");
      }
    }
  }
}