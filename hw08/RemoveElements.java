//Ryan Grace 
//Homework 08
//April 10 2018

import java.util.Scanner; //Import scanner and random classes
import java.util.Random;

public class RemoveElements{ 
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(num); //number one
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index: ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
    
    System.out.print("Enter the target value: ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  public static int[] randomInput(int[] inputArray){
    inputArray = new int [10];
    Random ranNum = new Random();
    for (int i = 0; i < inputArray.length; i++){
      int fill = ranNum.nextInt(10); //Generates random numbers and assigns them to variable fill 
      inputArray[i] = fill; //populates array 
    }
    return inputArray; //returns array 
  }
  
  public static int[] delete( int[] list, int pos){
    if (pos > list.length){ //Check to make sure user input is acceptable
      System.out.println("Position is out of bounds of array.");
      return list;
    }
    int[] outputArray;
    outputArray = new int [list.length-1]; //Creates array 
    int count = 0;
    for (int i = 0; i < outputArray.length; i++){
      if (i == pos){
        count++; //Checks to see if position has been reached yet
      }
      if (count == 0){ //If position has not been reached, array copies as normal 
        outputArray[i] = list[i];
      }
      if (count == 1){ //If position has been reached, array skips over the position (i+1) and continues on
        outputArray[i] = list [i+1];
      }
    }
    return outputArray;
  }
  
  public static int[] remove (int[] list, int target){
    int count = 0;
    for (int j = 0; j<list.length; j++){  //For loop goes and checks to see if and how many times the target appears in the list
      if (target == list[j]){
        count++;
      }
    }
    if (count == 0){
        System.out.println("Target is not in list.");
        return list; //Returns original array 
    }
    System.out.println("The target value appears "+count+" time(s) in the list.");
    int[] diffArray = new int [list.length - count]; //sets length of array to be returned, shortened by the no. of times target appears
    int j = 0;
    for (int i = 0; i < list.length; i++){
      if (target == list [i]){
        continue; //If the target appears, loop continues through without altering new array 
      }
      else{
      diffArray[j] = list[i]; //If position "i" doesn't contain target, the val is copied into new array 
        j++; //Array position moves forward only when value is not the target 
      }
    }
    return diffArray; //Returns new array without target values 
  }
}
