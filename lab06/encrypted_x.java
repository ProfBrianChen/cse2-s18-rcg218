//Ryan Grace
//March 22 2019
//Encrypted X - Lab 06

import java.util.Scanner;

public class encrypted_x {
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.println("Please enter an integer from 1 to 100.");
   
    while (!scan.hasNextInt()){  //test to see if input is an integer){ //if input is not an integer, request it again
      System.out.println("Bad input. Enter an integer from 1 to 100.");
      scan.next(); //request input again
    }
    int size = scan.nextInt();//assign input to variable size
    if (size < 1 || size > 100){  //check to see if in parameters
      System.out.println("Bad input. Enter an integer from 1 to 100.");
      scan.hasNextInt();
    }
    for (int i = 0; i < size; i++ ){ //outer for loop, moves vertically
      for (int j = 0; j < size; j++){ //inner for loop, moves hortizontally
        if (i == j){
          System.out.print(" "); //prints space when counters (i & j) are equal
        }
        else if (size-i == j + 1){ 
          System.out.print(" ");
        }
        else {
          System.out.print("*"); //Fills remainder
        }
      }
      System.out.println("");//continues to next line
    }
  }
}