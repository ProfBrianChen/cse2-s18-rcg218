//Ryan Grace
//CSE Lab 4
//Feb 16 2018
//This lab uses a random number generator to produce different cards out of a deck 
//I realize I did not follow the directions perfectly (I didn't use a random num from 1 to 52), but the result is the same
public class CardGenerator {
  public static void main(String[] args){
    //Below, I assigned default string variables that will later be reassigned using the random num generator 
    String suit = "default";  
    String card = "default";
    int suitNum = (int) (Math.random() * 4); 
    //Random number generator produces four outputs (0,1,2,3). 
    //Math.random produces a double, so it is very important we assign it as an int
    if (suitNum == 0){ 
      suit = "Spades"; //Assigns the formerly "default" string variable (suit) to "Spades"
    }
    else if ( suitNum == 1 ){
      suit = "Hearts"; //Assigns the formerly "default" string variable (suit) to "Hearts"
    }
    else if ( suitNum == 2 ){
      suit = "Clubs"; //Assigns the formerly "default" string variable (suit) to "Hearts"
    }
    else if ( suitNum == 3 ){
      suit = "Diamonds"; //Assigns the formerly "default" string variable (suit) to "Diamonds"
    }
    int cardNum = (int) (Math.random() * 13); //Creates an integer from 0-12, assigned to variable cardNum
    switch ( cardNum ){ //Switch statement takes the randomly generated number and assigns it to one of thirteen possible card outcomes
      case 0:
        card = "2";
        break;
      case 1:
        card = "3";
        break;
      case 2:
        card = "4";
        break;
      case 3:
        card = "5";
        break;
      case 4:
        card = "6";
        break;
      case 5:
        card = "7";
        break;
      case 6:
        card = "8";
        break;
      case 7:
        card = "9";
        break;
      case 8:
        card = "10";
        break;
      case 9:
        card = "Jack";
        break;
      case 10:
        card = "Queen";
        break;
      case 11:
        card = "King";
        break;
      case 12:
        card = "Ace";
        break;
    }
    System.out.println("You picked the "+card+" of "+suit+"."); //Uses the reassigned "default" variables tp produce the desired output
    }
  }  