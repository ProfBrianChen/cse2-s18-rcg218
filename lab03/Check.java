//Ryan Grace
//CSE2 Lab 3 - Splitting the Check
//February 9th 2018
import java.util.Scanner; //import scanner class
//
public class Check {
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Please enter the original cost of the check in the form xx.xx: "); //prompt the user to enter check total 
    double checkCost = myScanner.nextDouble(); //user enters double value for check total
    System.out.println("Please enter the percentage tip you wish to pay as a whole number (in the form xx): "); //prompt user to enter tip
    double tipPercent = myScanner.nextDouble(); //user enters tip percent
    tipPercent /= 100;
    System.out.println("Please enter the number of people that will be splitting the check: ");
    int numPeople = myScanner.nextInt(); 
    //
    double totalCost = (int) (100 * checkCost * (1 + tipPercent)) / 100.00;
    double costPerPerson = totalCost / numPeople;
    int dollars = (int) costPerPerson;
    int dimes = (int) (costPerPerson * 10) % 10;
    int pennies = (int) (costPerPerson * 100) % 10;
    System.out.println("The total cost of the meal including tax was $"+totalCost+"");
    System.out.println("Each person in the group owes $"+dollars + '.' + dimes + pennies+"");
  }
}