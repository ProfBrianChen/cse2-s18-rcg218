//Ryan Grace
//April 26, 2018 
//Homework 10 Late Submission
//I really do not understand this stuff. The code compiles, but I have runtime errors all over the place. Sorry.

import java.util.Random; //Import random number generator 

public class RobotCity{
  static Random ranNum = new Random(); //name the random num generator outside main method for flexibility 
  public static void main(String[] args){
    int [][] cityArray = buildCity(); //Create arrat and populate it via the buildCity method
    display(cityArray);
    System.out.println();
    int k = ranNum.nextInt(10);
    System.out.println("K: "+k);
   // invade(k, cityArray);
    display(cityArray);
    for (int i = 0; i < 5; i++){
      update(cityArray);
    }
    System.out.println("Array has updated 5 times.");
    display(cityArray);
  }
  public static int[][] buildCity(){
    int rows = ranNum.nextInt(6) + 10; //Randow number of rows 
    System.out.println("Rows: "+rows);
    int cols = ranNum.nextInt(6) + 10; //Random number of columns
    System.out.println("Cols: "+cols);
    int [][] cityArray = new int [rows][cols];
    for(int i = 0; i < cityArray.length; i++){
      for (int j = 0; j < cityArray[i].length; j++){
        cityArray[i][j] = ranNum.nextInt(900) + 100; //populated array with numbers between 100 and 999
      }
    }
    return cityArray;
  }
  
  public static void display(int [][] cityArray){
    for (int i = 0; i < cityArray.length; i++){
      for(int j = 0; j < cityArray[i].length; j++){ 
        System.out.print(cityArray[i][j]+" "); //Prints out each array within the greater array 
      } 
      System.out.println();
    }
    return;
  }
  public static void invade(int k, int[][]cityArray){
    for (int r = 0; r < k; r++){
      int colIndex = ranNum.nextInt(cityArray.length);
      int rowIndex = ranNum.nextInt(cityArray[0].length); //I can choose a random array in the multidimensional array because it is rectangular
        if (cityArray[rowIndex][colIndex] > 0){
        cityArray[rowIndex][colIndex] *= -1; //If a randomly selected point in the 2d array is not already negative, then it becmes negative
        }
        else { //If it the coordinates are a repeat, then the iteration is done again, hence the deincrement of r
          r--;
          continue;
        }
    }
  }
  public static void update(int[][] cityArray){
    for (int i = 0; i < cityArray.length; i++){ //lol i had no idea what I was doing here, good thing it's not on the 3rd exam
      for(int j = 0; j < cityArray[i].length; j++){ 
        if (cityArray[i][j] < 0){ 
          cityArray[i][j+1] *= -1;
        }
      }
    }
  }
}