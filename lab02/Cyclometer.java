//Ryan Grace 
//Feb 2, 2018
//CSE Lab 2 
// This lab practices arithmetic calculations by calculating the distance and time of two bike trips
public class Cyclometer{
  //main method required for every Java Program
  public static void main(String[] args){
    int secsTrip1 = 480; //beginning of input data
    int secsTrip2 = 3220;
    int countsTrip1 = 1561;
    int countsTrip2 = 9037; //counts represents the number of front wheel rotations on the trip 
    //end of input data
    //Begin constants below
    double wheelDiameter = 27.0;
    double PI = 3.14159; 
    int feetPerMile = 5280;
    int inchesPerFoot = 12;
    double secondsPerMinute = 60;
    //end of constants 
    double distanceTrip1 = countsTrip1 * (wheelDiameter * PI) / (feetPerMile * inchesPerFoot); 
    double distanceTrip2 = countsTrip2 * (wheelDiameter * PI) / (feetPerMile * inchesPerFoot);
           //above calculates distance in inches (pi * diameter * counts) and then converts to miles 

    double totalDistance = distanceTrip2 + distanceTrip1;
      //above adds the two distance together to get total distance 
    System.out.println("Trip 1 took "+secsTrip1 / secondsPerMinute+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+secsTrip2 / secondsPerMinute+" minutes and had "+countsTrip2+" counts.");
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
    System.out.println("Trip 2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance was "+totalDistance+" miles.");
      //above prints out the newly calculated information 
    } //end of main method
  } //end of class