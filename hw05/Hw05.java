//Ryan Grace
//March 5 2018
//Scanner and loops homework 
//The purpose of this lab is to identify what type of input the user provided and to weed out bad input using while loops and the scanner class

import java.util.Scanner;  //Import Scanner

public class Hw05{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); //Create Scanner titled myScanner
    String courseName, professorName, deptName; //declare all string variables
    int courseNumber, startTime, classSize, weekly;
    while ( true ){ //create infinite loop`
      System.out.println("What is the name of your class?");
      if (myScanner.hasNextLine()){ //Recieve input and check to see if it meets criteria of being a string.
        courseName = myScanner.nextLine(); //Assign the user's input to variable. I used nextLine() instead of next() in case the user entered multiple words
        break;   //Excape from infinite loop
      }
      else {
        System.out.println("Error. Please enter class name again.");
        myScanner.nextLine(); //clear scanner 
      }
      //I repeated the same process of checking the input for the following5 questions.
      //The only difference was I did not use nextLine in instances where the user would not enter multiple words.
      //An example of this is when the user is asked for an integer. This did not require the use of nextLine()
    }
    while ( true ){
      System.out.println("What department offers the class you are taking?");
      if ( myScanner.hasNextLine() ){
        deptName = myScanner.nextLine();
        break;
      }
      else {
        System.out.println("Error.");
        myScanner.nextLine();
      }
    }
    while ( true ){
      System.out.println("What is your course number?");
      if ( myScanner.hasNextInt() ){
        courseNumber = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Error. Please enter an integer for course number.");
        myScanner.next();
      }
    }
    while ( true ){
      System.out.println("How many times does your course meet per week?");
      if ( myScanner.hasNextInt() ){
        weekly = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Error. Please enter an integer.");
        myScanner.next();
      }
    }
    while ( true ){
      System.out.println("What time does your class start?");
      if ( myScanner.hasNextInt() ){
        startTime = myScanner.nextInt();
        break;
      }
      else {
        System.out.println("Error. Please sumbit an integer for time. For example 10:15 AM would be 1015.");
        myScanner.next();
      }
    }
    while ( true ){
      System.out.println("How many students are in your class?");
      if ( myScanner.hasNextInt() ){
        classSize = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Error. Enter an integer.");
        myScanner.next();
      }
    }
    //Print out the results in an organized fashion
    System.out.println("Course Name: "+courseName);
    System.out.println("Department Name: "+deptName);
    System.out.println("Course Number: "+courseNumber);
    System.out.println("Start Time: "+startTime);
    System.out.println("Class Size: "+classSize+" students.");
    System.out.println("This class meets "+weekly+" times per week.");
  }
}